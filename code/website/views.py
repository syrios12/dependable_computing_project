from flask import Flask, render_template, jsonify, request
from simulation import atomic_broadcast as ab
from helpers import crossdomain

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/simulation/<int:n>', methods=['POST'])
@crossdomain(origin='*')
def run_simulation(n=1):
    n_proc = int(request.form['n_proc'])
    delta = int(request.form['delta'])
    n_failures = int(request.form['n_failures'])
    chan_prob = float(request.form['chan_prob'])
    proc_prob = float(request.form['proc_prob'])
    if n_proc > 20: return jsonify({'error': 'too many processors'})
    model = ab.SimulateBroadcast(n_processors=n_proc,
                                 delta=delta,
                                 f=n_failures,
                                 prob_ch_dies=chan_prob,
                                 prob_proc_dies=proc_prob)
    results = model.run_simulation(n)
    return jsonify(results.__dict__)
