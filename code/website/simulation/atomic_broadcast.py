import random
import simpy
import numpy as np
from collections import namedtuple

# Assuming no epsilon; once processor or channel dead, dead;
# Delta must be greater than 1
# Assume 1 second of delta for processing


class SimulateBroadcast(object):
    def __init__(self, n_processors, delta, f, prob_proc_dies, prob_ch_dies, verbose=False):
        self.n_processors = n_processors
        self.delta = delta
        self.f = f
        self.prob_proc_dies = prob_proc_dies
        self.prob_ch_dies = prob_ch_dies
        self.n_faults = 0
        self.verbose = verbose

    def run_simulation(self, n):
        simulation_stats = namedtuple('Simulation_Statistics',
                                      ['n_failed_proc', 'n_failed_ch', 'successful', 'term_time'])
        n_failed_proc = []
        n_failed_ch = []
        successful = []
        term_time = []
        for i in range(n):
            pipe = self.run_single()
            n_failed_proc.append(pipe.n_dead_procs)
            n_failed_ch.append(len(pipe.dead_channels))
            successful.append(pipe.success)
            term_time.append(pipe.term_time)
        return simulation_stats(n_failed_proc, n_failed_ch, successful, term_time)


    def run_single(self):
        env = simpy.Environment()
        bc_pipe = BroadcastPipe(env, self.f, self.delta, self.prob_ch_dies, self.prob_proc_dies, self.n_processors)
        # create pipes and processors
        for i in range(self.n_processors):
            bc_pipe.get_output_conn()
            env.process(Processor(env, bc_pipe, i).consume(self.verbose))
        env.run()
        return bc_pipe


class BroadcastPipe(object):
    def __init__(self, env, f, delta, prob_ch_dies, prob_proc_dies, n_processors, capacity=simpy.core.Infinity):
        self.env = env
        self.capacity = capacity
        self.pipes = []
        self.dead_channels = []
        self.n_dead_procs = 0
        self.success = 0
        self.term_time = 0
        self.f = f
        self.delta = delta
        self.n_processors = n_processors
        self.prob_ch_dies = prob_ch_dies
        self.prob_proc_dies = prob_proc_dies
        self.faults = simpy.Container(self.env, capacity=self.f)
        self.n_proc_rcvd = np.zeros(self.n_processors)

    def put(self, value):
        """Broadcast a *value* to all receivers."""
        if not self.pipes:
            raise RuntimeError('There are no output pipes.')
        events = [store.put(value) for store in self.pipes]
        return self.env.all_of(events)  # Condition event for all "events"

    def get_output_conn(self):
        """Get a new output connection for this broadcast pipe.

        The return value is a :class:`~simpy.resources.store.Store`.

        """
        pipe = simpy.Store(self.env, capacity=self.capacity)
        self.pipes.append(pipe)
        return pipe


class Processor(object):
    def __init__(self, env, in_pipe, process_id):
        self.is_dead = False
        self.env = env
        self.in_pipe = in_pipe
        self.process_id = process_id
        self.name = 'Processor{}'.format(self.process_id)
        
    def consume(self, verbose):

        # processor zero responsible for starting things off
        if self.process_id == 0:
            # wait DELTA time
            yield self.env.timeout(1)
            max_channel_send = self.in_pipe.f + 1
            for i in range(max_channel_send):
                if not self.is_dead:
                    self.update(i, verbose)
                else:
                    max_channel_send = i
                    break

            # send to everyone but self
            for pipe in self.in_pipe.pipes[1:]:
                msg = (self.env.now, 1, range(max_channel_send))
                pipe.put(msg)
            # if died, kill
            if self.is_dead:
                self.in_pipe.n_dead_procs += 1
                self.env.exit()

        # keep doing until broadcast terminates
        while True: 
            # Get the event out of your pipe
            msg = yield self.in_pipe.pipes[self.process_id].get()
            
            channels_not_broken = [x for x in msg[2] if x not in self.in_pipe.dead_channels]

            # Wait necessary time
            yield self.env.timeout(max(msg[0] + self.in_pipe.delta * msg[1] - 1 - self.env.now, 0))

            # Print that you got message
            if verbose:
                print('at time {0}: {1} received message a hop {2} message on channel(s) {3}'
                      .format(self.env.now, self.name, msg[1], channels_not_broken))

            # check if broadcast failed
            if not channels_not_broken:
                if np.sum(self.in_pipe.n_proc_rcvd) == self.in_pipe.n_processors - 1:
                    self.in_pipe.success = 1
                    self.in_pipe.term_time = self.env.now - 1
                    if verbose:
                        print("Broadcast Succeeded @ {}".format(self.env.now))
                else:
                    if verbose:
                        print("Broadcast failed @ {}".format(self.env.now))
                self.env.exit()
            # check off received
            else:
                if self.in_pipe.n_proc_rcvd[self.process_id] == 0:
                    self.in_pipe.n_proc_rcvd[self.process_id] = 1

            # Wait for others - processing time
            yield self.env.timeout(1)

            # Check if need to forward
            max_channel = np.max(channels_not_broken)
            max_channel_send = self.in_pipe.f - msg[1]
            if max_channel < max_channel_send:
                for i in range(max_channel+1, max_channel_send):
                    if not self.is_dead:
                        self.update(i, verbose)
                    else:
                        max_channel_send = i
                        break
                    
                # broadcast to all
                if verbose:
                    print("{0} sent messages on channels {1}".format(self.name, range(max_channel+1, max_channel_send)))
                msg = (self.env.now, msg[1] + 1, range(max_channel+1, max_channel_send))
                self.in_pipe.put(msg)
            # If don't need to forward then you are done
            else:
                if verbose:
                    print("Broadcast Terminated @ {0}".format(self.env.now))
                self.in_pipe.success = 1
                self.in_pipe.term_time = self.env.now - 1
                self.env.exit()
            # if died, kill
            if self.is_dead:
                self.in_pipe.n_dead_procs += 1
                self.env.exit()
                
    def update(self, channel, verbose):
        
        if random.random() <= self.in_pipe.prob_ch_dies \
                and self.in_pipe.faults.level < self.in_pipe.f \
                and channel not in self.in_pipe.dead_channels:
            if verbose:
                print("Channel {0} died @ {1}".format(channel, self.env.now))
            self.in_pipe.faults.put(1)
            if verbose:
                print("Number of Faults: {}".format(self.in_pipe.faults.level))
            self.in_pipe.dead_channels.append(channel)
        
        # processor can also die
        if random.random() <= self.in_pipe.prob_proc_dies \
                and self.in_pipe.faults.level < self.in_pipe.f:
            if verbose:
                print("{0} died @ {1}!".format(self.name, self.env.now))
            self.in_pipe.faults.put(1)
            if verbose:
                print("Number of Faults: {}".format(self.in_pipe.faults.level))
            self.is_dead = True
